﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestBitBucket.Startup))]
namespace TestBitBucket
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
